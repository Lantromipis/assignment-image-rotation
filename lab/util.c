#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

void out( const char* msg, ... ) {
    va_list args;
    va_start (args, msg);
    vfprintf(stdout, msg, args);
    va_end (args);
}

void print_read_msg(enum read_status status){
    switch (status) {
        case READ_OK:
            out("file was successfully read\n");
            break;
        case READ_INVALID_HEADER:
            err("invalid file header\n");
            break;
        case READ_HEADER_ERROR:
            err("error reading file header\n");
            break;
        case READ_UNEXPECTED_EOF:
            err("file ended unexpectedly\n");
            break;
        case READ_FILE_ERROR:
        default:
            err("error while reading file\n");
            break;
    }
}
void print_write_msg(enum write_status status){
    switch (status) {
        case WRITE_OK:
            out("data written successfully to file\n");
            break;
        case WRITE_ERROR:
        default:
            err("error writing data to file\n");
            break;
    }
}
void print_save_msg(enum save_file_status status){
    switch (status) {
        case SAVE_OK:
            out("image was saved successfully\n");
            break;
        case SAVE_FAIL:
        default:
            err("error saving image to file\n");
            break;
    }
}
void print_open_msg(enum open_file_status status){
    switch (status) {
        case OPEN_OK:
            out("image was opened successfully\n");
            break;
        case OPENED_BUT_READ_FAIL:
            err("image file was opened successfully, but error while reading\n");
            break;
        case OPEN_FAIL:
        default:
            err("error opening image from file\n");
            break;
    }
}
