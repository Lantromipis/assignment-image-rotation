#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


void bmp_header_print( struct bmp_header const* header, FILE* f ) {
   FOR_BMP_HEADER( PRINT_FIELD )
}

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header = { 0 };
    if(!read_header(in, &header)){
        if(feof(in)) return READ_UNEXPECTED_EOF;
        return READ_FILE_ERROR;
    }

    const uint32_t height = header.biHeight;
    const uint32_t width = header.biWidth;

    img->width=width;
    img->height=height;
    img->data=malloc(sizeof (struct pixel)*height*width);

    const size_t padding = (4 - (width * 3 % 4))%4;

    for(size_t i=0;i<height;i++){
        if(fread(&(img->data[i * width]), sizeof (struct pixel), width, in) < 1){
            free(img->data);

            if(feof(in)) return READ_UNEXPECTED_EOF;
            return READ_FILE_ERROR;
        }
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    const uint8_t empty_byte = 0;

    struct bmp_header header = create_bmp_header(img->width, img->height);
    if(fwrite(&header, sizeof(struct bmp_header), 1, out)<1){
        fclose(out);
        return WRITE_ERROR;
    }

    const size_t padding = (4 - (img->width * 3 % 4))%4;

    for (size_t i = 0; i < img->height; i++) {
        if(fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) < 1){
            fclose(out);
            return WRITE_ERROR;
        }
        fwrite(&empty_byte, sizeof empty_byte, padding, out);
    }
    fclose(out);
    return WRITE_OK;
}

enum open_file_status open_bmp_from_file( const char* filename, struct image* image){
    FILE* in = fopen(filename, "rb");
    if(!in) {
        fclose(in);
        return OPEN_FAIL;
    }

    enum read_status status = from_bmp(in, image);
    print_read_msg(status);

    if(status != READ_OK){
        fclose(in);
        return OPENED_BUT_READ_FAIL;
    } else{
        fclose(in);
        return OPEN_OK;
    }
}

enum save_file_status save_as_bmp( const char* filename, struct image* image){
    FILE* out = fopen(filename, "wb");
    if(!out){
        fclose(out);
        return SAVE_FAIL;
    }

    enum write_status status = to_bmp(out, image);
    print_write_msg(status);

    if(status != WRITE_OK){
        fclose(out);
        return SAVE_FAIL;
    }
    else {
        fclose(out);
        return SAVE_OK;
    }
}


//дефолтный хедер по материалам интернета (например вот http://www.fastgraph.com/help/bmp_header_format.html)
struct bmp_header create_bmp_header(uint64_t width, uint64_t height){
    return (struct bmp_header) {
            .bfType = 0x4d42,
            .bfileSize = width*height*24,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeof(struct bmp_header) + width*height*24,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

}
