#ifndef _UTIL_H_
#define _UTIL_H_

_Noreturn void err( const char* msg, ... );
void out( const char* msg, ... );

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_ERROR,
    READ_HEADER_ERROR,
    READ_UNEXPECTED_EOF
    /* коды других ошибок  */
};

/*  serializer   */
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

/*  открытие файла  */
enum open_file_status {
    OPEN_OK =0,
    OPEN_FAIL,
    OPENED_BUT_READ_FAIL,
};

/*  запись файла  */
enum save_file_status {
    SAVE_OK =0,
    SAVE_FAIL,
};

void print_read_msg(enum read_status status);
void print_write_msg(enum write_status status);
void print_save_msg(enum save_file_status status);
void print_open_msg(enum open_file_status status);


#endif
