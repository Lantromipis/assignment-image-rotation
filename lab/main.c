#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "util.h"


void usage() {
    fprintf(stderr, "Usage: ./rotate_bmp BMP_FILE_NAME BMP_ROTATED_FILE_NAME\n");
}

int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n" );
    if (argc > 3) err("Too many arguments \n" );

    struct image* img = malloc(sizeof (struct image));

    open_bmp_from_file(argv[1], img);

    out("rotating image...\n");

    struct image rotated_img = rotate(*img);

    out("image rotated\n");

    save_as_bmp(argv[2], &rotated_img);
    //save_as_bmp(argv[2], img);

    return 0;
}
