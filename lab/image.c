//
// Created by Lantromipis on 07.01.2021.
//

#include "image.h"

struct image rotate( struct image const source ){
    const uint64_t width = source.width;
    const uint64_t height = source.height;

    struct image rotated_img = {
            height,
            width,
            malloc(sizeof (struct pixel) *height*width)
    };

    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            rotated_img.data[j*height + (height-1-i)] = source.data[i*width+j];
        }
    }

    return rotated_img;
}