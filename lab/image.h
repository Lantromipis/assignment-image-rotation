//
// Created by Lantromipis on 07.01.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <malloc.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
